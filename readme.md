## Assignment App
### How to run
#### Run with Gradle
Test:

`~>./gradlew clean test `

Build:

`~>./gradlew clean build`

Run directly:

`./gradlew bootRun`

#### Run with Docker

Build docker image:

`~>./gradlew dockerBuildImage`

Run docker image:

`~>docker run --name assignment-app -ti --rm -p 8080:8080 assignment-app`

### Swagger UI
http://localhost:8080/swagger-ui.html

### Design decisions
#### Transactional tests
For simplicity, tests are transactional and using Mock MVC. This is much faster to develop (transactional tests mean simple rollback, therefore independent tests), but the downside is that the transactional context is different than in a live application
#### Validation
For simplicity, there is little validation on entities. Proper valitation would be done on the database level as well, but testing that properly requires real transactional contex
#### Database
In the requirements there's a point `Provide a storage solution for persisting the API’s data`. I hope that in-memory h2 satisfies that, even though the data is lost during restart. Normally I'd clarify that with the business before implementation, it wasn't possible in this case.
#### Exceptions
For simplicity, no custom exceptions
#### DDD
Order and Product are entities and aggregate roots. Because these are different aggregates, I don't have a direct reference between Order and Product, which allowed for an interesting use case: amount calculation of the original price from the moment of order placement, and not the current price.

I considered using Spring Data JDBC to provide immutability, but I'm not proficient enough in this technology so I was worried I'd run out of time. It would probably be a better tool for this task than JPA.

I also gave up on immutable DTOs, because of time constraints, even though recent Lombok works well with @Jacksonized and it allows immetable DTOs
