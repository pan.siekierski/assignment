package home.assignment.domain.order.model;

import home.assignment.domain.product.model.Price;
import java.math.BigDecimal;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderItem {

  private Long productId;
  private int amount;
  private Price price;

  public BigDecimal totalAmount() {
    return price.getValue().multiply(new BigDecimal(amount));
  }
}
