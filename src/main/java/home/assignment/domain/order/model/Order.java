package home.assignment.domain.order.model;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "orders") // order is a sql keyword - convention on how to name it on a team level
@Data
@EntityListeners(AuditingEntityListener.class)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order {

  @Id
  @GeneratedValue
  private Long id;
  @CreatedDate
  private Instant createdAt;
  @Embedded
  private Email buyersEmail;
  @Default
  @ElementCollection
  private List<OrderItem> orderItems = new ArrayList<>();

  public Optional<OrderItem> getOrderItemByProductId(Long productId) {
    return orderItems.stream()
      .filter(orderItem -> orderItem.getProductId().equals(productId))
      .findFirst();
  }

  public BigDecimal totalAmount() {
    return orderItems.stream()
      .map(OrderItem::totalAmount)
      .reduce(BigDecimal.ZERO, BigDecimal::add);
  }
}
