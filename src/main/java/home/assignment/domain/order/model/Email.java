package home.assignment.domain.order.model;

import javax.persistence.Embeddable;
import javax.validation.ValidationException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;

@Embeddable
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Email {

  private String value;

  public static Email of(String emailString) {
    validateEmail(emailString);
    return new Email(emailString);
  }

  private static void validateEmail(String emailString) {
    if (!new EmailValidator().isValid(emailString, null)) {
      throw new ValidationException("Email invalid: " + emailString);
    }
  }

}
