package home.assignment.domain.order;

import home.assignment.domain.order.dtos.OrderDto;
import home.assignment.domain.order.dtos.OrderItemDto;
import home.assignment.domain.order.dtos.PlaceOrderItemRequest;
import home.assignment.domain.order.dtos.PlaceOrderRequest;
import home.assignment.domain.order.dtos.PlaceOrderResponse;
import home.assignment.domain.order.model.Email;
import home.assignment.domain.order.model.Order;
import home.assignment.domain.order.model.OrderItem;
import home.assignment.domain.product.model.Price;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OrderMapper {

  default Order map(PlaceOrderRequest request, Map<Long, Price> priceMap) {
    List<OrderItem> orderItems = request.getOrderItems().stream()
      .map(item -> mapOrderItem(item, priceMap))
      .collect(Collectors.toList());

    return Order.builder()
      .buyersEmail(Email.of(request.getBuyersEmail()))
      .orderItems(orderItems)
      .build();
  }

  private OrderItem mapOrderItem(PlaceOrderItemRequest item, Map<Long, Price> priceMap) {
    return OrderItem.builder()
      .amount(item.getAmount())
      .productId(item.getProductId())
      .price(priceMap.get(item.getProductId()))
      .build();
  }

  PlaceOrderResponse mapToPlaceOrderResponse(Order entity);

  @Mapping(target = "buyersEmail", source = "buyersEmail.value")
  OrderDto map(Order order);

  default OrderItemDto map(OrderItem orderItem) {
    return OrderItemDto.builder()
      .productId(orderItem.getProductId())
      .price(orderItem.getPrice().getValue())
      .amount(orderItem.getAmount())
      .build();
  }
}
