package home.assignment.domain.order.dtos;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class PlaceOrderItemRequest {

  @NotNull
  private Long productId;
  @Min(1)
  private int amount;
}
