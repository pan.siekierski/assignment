package home.assignment.domain.order.dtos;

import lombok.Data;

@Data
public class PlaceOrderResponse {

  private Long id;
}
