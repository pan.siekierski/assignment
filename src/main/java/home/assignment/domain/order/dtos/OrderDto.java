package home.assignment.domain.order.dtos;

import java.time.Instant;
import java.util.List;
import lombok.Data;

@Data
public class OrderDto {

  private Long id;
  private Instant createdAt;
  private String buyersEmail;
  private List<OrderItemDto> orderItems;
}
