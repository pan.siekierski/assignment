package home.assignment.domain.order.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import home.assignment.application.PriceSerializer;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemDto {

  private Long productId;
  private int amount;
  @JsonSerialize(using = PriceSerializer.class)
  private BigDecimal price;
}
