package home.assignment.domain.order.dtos;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class PlaceOrderRequest {

  @Valid
  @NotEmpty
  private List<PlaceOrderItemRequest> orderItems;
  @NotEmpty
  private String buyersEmail;
}
