package home.assignment.domain.order;

import home.assignment.domain.order.model.Order;
import java.time.Instant;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrderRepository extends JpaRepository<Order, Long> {

  @Query("select o from Order o where createdAt >= :dateFrom and createdAt <= :dateTo")
  List<Order> findByDatesBetreenInclusive(Instant dateFrom, Instant dateTo);
}
