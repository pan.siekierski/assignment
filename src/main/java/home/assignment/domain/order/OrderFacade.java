package home.assignment.domain.order;

import home.assignment.domain.order.dtos.OrderDto;
import home.assignment.domain.order.dtos.PlaceOrderItemRequest;
import home.assignment.domain.order.dtos.PlaceOrderRequest;
import home.assignment.domain.order.dtos.PlaceOrderResponse;
import home.assignment.domain.order.model.Order;
import home.assignment.domain.product.ProductFacade;
import home.assignment.domain.product.model.Price;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class OrderFacade {

  private final OrderMapper orderMapper;
  private final OrderRepository orderRepository;
  private final ProductFacade productFacade;

  public PlaceOrderResponse place(PlaceOrderRequest request) {
    Map<Long, Price> priceMap = getPriceMap(request);
    Order order = orderMapper.map(request, priceMap);
    Order savedOrder = orderRepository.save(order);
    return orderMapper.mapToPlaceOrderResponse(savedOrder);
  }

  private List<Long> extractProductIds(PlaceOrderRequest request) {
    return request.getOrderItems().stream().map(PlaceOrderItemRequest::getProductId).collect(
      Collectors.toList());
  }

  private Map<Long, Price> getPriceMap(PlaceOrderRequest request) {
    List<Long> productIds = extractProductIds(request);
    return productFacade.getPriceMap(productIds);
  }

  public List<OrderDto> findOrders(ZonedDateTime dateFrom,
    ZonedDateTime dateTo) {
    List<Order> foundOrders = orderRepository
      .findByDatesBetreenInclusive(dateFrom.toInstant(), dateTo.toInstant());
    return foundOrders.stream().map(orderMapper::map).collect(Collectors.toList());
  }
}
