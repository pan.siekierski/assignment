package home.assignment.domain.product.model;

import java.time.Instant;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Data
@EntityListeners({AuditingEntityListener.class})
public class Product {

  @Id
  @GeneratedValue
  private Long id;
  private String name;
  @Embedded
  private Price price;
  @CreatedDate
  private Instant createdAt;
  private boolean active = true;

  public void setInactive() {
    this.active = false;
  }
}
