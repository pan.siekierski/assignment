package home.assignment.domain.product.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Access(AccessType.FIELD) // must be set explicitly, otherwise nested embeddable doesn't work
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Price {

  private BigDecimal value;

  public static Price from(BigDecimal price) {
    BigDecimal value = price.setScale(2, RoundingMode.UNNECESSARY);
    if (value.compareTo(BigDecimal.ZERO) <= 0) {
      throw new ArithmeticException("Price can't be zero or negative");
    }
    return new Price(value);
  }
}
