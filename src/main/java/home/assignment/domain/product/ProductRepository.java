package home.assignment.domain.product;

import home.assignment.domain.product.model.Product;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepository extends JpaRepository<Product, Long> {

  List<Product> findByActiveTrue();

  @Query("select p from Product p "
    + "where p.id = :id "
    + "and p.active = true")
  Optional<Product> findActiveById(Long id);
}
