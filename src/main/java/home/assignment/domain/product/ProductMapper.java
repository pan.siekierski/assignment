package home.assignment.domain.product;

import home.assignment.domain.product.dtos.CreateProductRequest;
import home.assignment.domain.product.dtos.ProductResponse;
import home.assignment.domain.product.dtos.UpdateProductRequest;
import home.assignment.domain.product.model.Price;
import home.assignment.domain.product.model.Product;
import java.math.BigDecimal;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProductMapper {

  Product map(CreateProductRequest createProductRequest);

  @Mapping(target = "price", source = "price.value")
  ProductResponse map(Product entity);

  default Price map(BigDecimal price) {
    return Price.from(price);
  }

  void map(@MappingTarget Product product, UpdateProductRequest updateRequest);
}
