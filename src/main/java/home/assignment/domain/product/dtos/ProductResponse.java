package home.assignment.domain.product.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import home.assignment.application.PriceSerializer;
import java.math.BigDecimal;
import java.time.Instant;
import lombok.Data;

@Data
public class ProductResponse {

  private Long id;
  private String name;
  @JsonSerialize(using = PriceSerializer.class)
  private BigDecimal price;
  private Instant createdAt;
}
