package home.assignment.domain.product.dtos;

import java.math.BigDecimal;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateProductRequest {

  @NotEmpty
  private String name;

  @NotNull
  private BigDecimal price;

  public static CreateProductRequest of(String name, String price) {
    return new CreateProductRequest(name, new BigDecimal(price));
  }
}
