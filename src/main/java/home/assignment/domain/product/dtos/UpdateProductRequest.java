package home.assignment.domain.product.dtos;

import java.math.BigDecimal;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateProductRequest {

  @NotEmpty
  private String name;
  @NotNull
  private BigDecimal price;

  public static UpdateProductRequest of(String name, String price) {
    return new UpdateProductRequest(name, new BigDecimal(price));
  }
}
