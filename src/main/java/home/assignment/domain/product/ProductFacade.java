package home.assignment.domain.product;

import home.assignment.domain.product.dtos.CreateProductRequest;
import home.assignment.domain.product.dtos.ProductResponse;
import home.assignment.domain.product.dtos.UpdateProductRequest;
import home.assignment.domain.product.model.Price;
import home.assignment.domain.product.model.Product;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.validation.ValidationException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Transactional
public class ProductFacade {

  private final ProductMapper productMapper;
  private final ProductRepository productRepository;

  public ProductResponse create(CreateProductRequest request) {
    Product product = productMapper.map(request);
    Product savedProduct = productRepository.save(product);
    return productMapper.map(savedProduct);
  }

  public List<ProductResponse> findAllActive() {
    List<Product> products = productRepository.findByActiveTrue();
    return products.stream().map(productMapper::map).collect(Collectors.toList());
  }

  public ProductResponse update(Long productId, UpdateProductRequest updateRequest) {
    Product product = productRepository.findActiveById(productId)
      .orElseThrow(EntityNotFoundException::new);
    productMapper.map(product, updateRequest);
    Product savedProduct = productRepository.save(product);
    return productMapper.map(savedProduct);
  }

  public void softDelete(Long productId) {
    Product product = productRepository.findActiveById(productId)
      .orElseThrow(EntityNotFoundException::new);
    product.setInactive();
    productRepository.save(product);
  }

  public Map<Long, Price> getPriceMap(List<Long> productIds) {
    Map<Long, Price> priceMap = new HashMap<>();
    productIds.forEach(productId -> priceMap.put(productId, getPrice(productId)));
    return priceMap;
  }

  private Price getPrice(Long id) {
    return productRepository.findActiveById(id).orElseThrow(ValidationException::new).getPrice();
  }
}
