package home.assignment.application.web;

import home.assignment.domain.product.ProductFacade;
import home.assignment.domain.product.dtos.CreateProductRequest;
import home.assignment.domain.product.dtos.ProductResponse;
import home.assignment.domain.product.dtos.UpdateProductRequest;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ProductController.PRODUCTS_URI)
@AllArgsConstructor
public class ProductController {

  public static final String PRODUCTS_URI = "/products";
  private final ProductFacade productFacade;

  @PostMapping
  @ApiResponses({
    @ApiResponse(code = 201, message = "Created"),
    @ApiResponse(code = 400, message = "Bad request")
  })
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<ProductResponse> create(@Valid @RequestBody CreateProductRequest request) {
    ProductResponse result = productFacade.create(request);
    return new ResponseEntity<>(result, HttpStatus.CREATED);
  }

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<ProductResponse>> findAll() {
    return new ResponseEntity<>(productFacade.findAllActive(), HttpStatus.OK);
  }

  @PutMapping(path = "/{productId}")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Success"),
    @ApiResponse(code = 400, message = "Bad request"),
    @ApiResponse(code = 404, message = "Entity not found")
  })
  public ResponseEntity<ProductResponse> update(@PathVariable Long productId,
    @Valid @RequestBody UpdateProductRequest request) {
    ProductResponse result = productFacade.update(productId, request);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @DeleteMapping(path = "/{productId}")
  @ApiResponses({
    @ApiResponse(code = 204, message = "Deleted"),
    @ApiResponse(code = 404, message = "Entity not found")
  })
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public ResponseEntity<Void> delete(@PathVariable Long productId) {
    productFacade.softDelete(productId);
    return ResponseEntity.noContent().build();
  }

}
