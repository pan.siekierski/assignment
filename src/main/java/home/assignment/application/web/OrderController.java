package home.assignment.application.web;

import home.assignment.domain.order.OrderFacade;
import home.assignment.domain.order.dtos.OrderDto;
import home.assignment.domain.order.dtos.PlaceOrderRequest;
import home.assignment.domain.order.dtos.PlaceOrderResponse;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.time.ZonedDateTime;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(OrderController.ORDERS_URI)
@AllArgsConstructor
public class OrderController {

  public static final String ORDERS_URI = "/orders";
  private final OrderFacade orderFacade;

  @PostMapping
  @ApiResponses({
    @ApiResponse(code = 201, message = "Created"),
    @ApiResponse(code = 400, message = "Bad request")
  })
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<PlaceOrderResponse> place(@Valid @RequestBody PlaceOrderRequest request) {
    PlaceOrderResponse place = orderFacade.place(request);
    return new ResponseEntity<>(place, HttpStatus.CREATED);
  }

  @GetMapping
  @ApiResponses({
    @ApiResponse(code = 200, message = "Success"),
    @ApiResponse(code = 400, message = "Bad request")
  })
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<OrderDto>> getOrders(
    @RequestParam("dateFrom") @DateTimeFormat(iso = ISO.DATE_TIME) ZonedDateTime dateFrom,
    @RequestParam("dateTo") @DateTimeFormat(iso = ISO.DATE_TIME) ZonedDateTime dateTo) {
    List<OrderDto> orderList = orderFacade.findOrders(dateFrom, dateTo);
    return new ResponseEntity<>(orderList, HttpStatus.OK);
  }

}
