package home.assignment.application.web;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Slf4j
public class ExceptionHandlingController {

  @ExceptionHandler(ArithmeticException.class)
  @ResponseStatus(code = HttpStatus.BAD_REQUEST)
  public void badRequest(ArithmeticException ex) {
    logException(ex);
  }

  @ExceptionHandler({EntityNotFoundException.class, EmptyResultDataAccessException.class})
  @ResponseStatus(code = HttpStatus.NOT_FOUND)
  public void notFound(Exception ex) {
    logException(ex);
  }

  @ExceptionHandler({ValidationException.class})
  @ResponseStatus(code = HttpStatus.BAD_REQUEST)
  public void notFound(ValidationException ex) {
    logException(ex);
  }

  private void logException(Exception ex) {
    log.error("Intercepted exception: ", ex);
  }
}
