package home.assignment

import home.assignment.domain.product.ProductFacade
import home.assignment.domain.product.dtos.CreateProductRequest
import home.assignment.domain.product.dtos.ProductResponse
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import javax.transaction.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.data.auditing.DateTimeProvider
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

@SpringBootTest
@Import(TestConfig)
@ActiveProfiles("test")
@Transactional
@AutoConfigureMockMvc
class IntegrationTest extends Specification {

    protected static final ZonedDateTime DATE_TIME_NOW = ZonedDateTime.of(LocalDateTime.of(2020, 1, 1, 1, 1), ZoneId.of("UTC"))
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected DateTimeProvider dateTimeProvider
    @Autowired
    protected ProductFacade productFacade

    void setup() {
        dateTimeProvider.getNow() >> Optional.ofNullable(DATE_TIME_NOW)
    }

    protected ProductResponse createProduct(String name, String price) {
        def product = CreateProductRequest.of(name, price)
        productFacade.create(product)
    }
}
