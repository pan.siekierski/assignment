package home.assignment.application

import static home.assignment.application.web.ProductController.PRODUCTS_URI
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import home.assignment.IntegrationTest
import home.assignment.domain.product.ProductRepository
import home.assignment.domain.product.dtos.CreateProductRequest
import home.assignment.domain.product.dtos.ProductResponse
import home.assignment.domain.product.dtos.UpdateProductRequest
import home.assignment.domain.product.model.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders

class ProductIntegrationTest extends IntegrationTest {

    @Autowired
    ProductRepository productRepository

    @Autowired
    ObjectMapper mapper

    def "Should create a product"() {
        given:
        CreateProductRequest request = CreateProductRequest.of("name", "15.10")

        when:
        def responseString = mockMvc
                .perform(post(PRODUCTS_URI)
                        .content(mapper.writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn().response.contentAsString

        then: "response entity is correct"
        def responseProduct = mapper.readValue(responseString, ProductResponse)
        responseProduct.id != null
        responseProduct.createdAt == DATE_TIME_NOW.toInstant()
        responseProduct.name == "name"
        responseProduct.price.toString() == "15.10"

        and: "the entity is present in the database"
        def entity = getEntityById(responseProduct.id)
        entity.name == "name"
        entity.price.value == new BigDecimal("15.10")
        entity.createdAt == DATE_TIME_NOW.toInstant()
    }

    def "Should return 400 when price format is invalid"() {
        given:
        CreateProductRequest request = CreateProductRequest.of("name", "15.104")

        when:
        def response = mockMvc
                .perform(post(PRODUCTS_URI)
                        .content(mapper.writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON))

        then:
        response.andExpect(status().isBadRequest())
    }

    def "Should return 400 when name or price is null"() {
        given:
        CreateProductRequest request = new CreateProductRequest(name, price)

        when:
        def response = mockMvc
                .perform(post(PRODUCTS_URI)
                        .content(mapper.writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON))
        then:
        response.andExpect(status().isBadRequest())

        where:
        name   | price
        null   | new BigDecimal("14.23")
        "name" | null
    }

    def "Should retrieve a list of all products"() {
        given:
        def product1 = createProduct("name1", "3.40")
        createProduct("name2", "3.95")

        when:
        def response = mockMvc
                .perform(get(PRODUCTS_URI)
                        .contentType(MediaType.APPLICATION_JSON))

        then:
        response.andExpect(status().isOk())
        def content = response.andReturn().response.contentAsString
        def productList = mapper.readValue(content, new TypeReference<List<ProductResponse>>() {})

        productList.size() == 2
        def product1Response = getProductByName(productList, "name1")
        product1Response.price == product1.price
        product1Response.id != null
        product1Response.createdAt != null
    }

    def "Should update a chosen product"() {
        given:
        def originalProduct = createProduct("name", "3.40")
        def updateProductRequest = UpdateProductRequest.of("changed name", "5")

        when:
        def updatedResponse = mockMvc
                .perform(MockMvcRequestBuilders.put(PRODUCTS_URI + "/" + originalProduct.id)
                        .content(mapper.writeValueAsString(updateProductRequest))
                        .contentType(MediaType.APPLICATION_JSON))

        then: "verify response"
        updatedResponse.andExpect(status().isOk())

        then: "response entity is correct"
        def responseBody = updatedResponse.andReturn().response.contentAsString
        def responseProduct = mapper.readValue(responseBody, ProductResponse)
        responseProduct.id == originalProduct.id
        responseProduct.createdAt == DATE_TIME_NOW.toInstant()
        responseProduct.name == "changed name"
        responseProduct.price.toString() == "5.00"

        and: "the entity is present in the database"
        def entity = getEntityById(originalProduct.id)
        entity.name == "changed name"
        entity.price.value == new BigDecimal("5.00")
        entity.createdAt == DATE_TIME_NOW.toInstant()
    }

    def "Should return 400 when trying to update with null values"() {
        given:
        def product = createProduct("name", "3.40")
        def updateProductRequest = new UpdateProductRequest(name, price)

        when:
        def updatedResponse = mockMvc
                .perform(MockMvcRequestBuilders.put(PRODUCTS_URI + "/" + product.id)
                        .content(mapper.writeValueAsString(updateProductRequest))
                        .contentType(MediaType.APPLICATION_JSON))

        then: "verify response"
        updatedResponse.andExpect(status().isBadRequest())

        where:
        name   | price
        null   | new BigDecimal("14.23")
        "name" | null
    }

    def "Should return 404 when trying to update a non existing product"() {
        given:
        createProduct("name", "3.40")
        def updateProductRequest = UpdateProductRequest.of("changed name", "5")

        when:
        def updatedResponse = mockMvc
                .perform(MockMvcRequestBuilders.put(PRODUCTS_URI + "/49")
                        .content(mapper.writeValueAsString(updateProductRequest))
                        .contentType(MediaType.APPLICATION_JSON))

        then: "verify response"
        updatedResponse.andExpect(status().isNotFound())
    }

    def "Should successfully delete product"() {
        given:
        def product = createProduct("name", "3.40")
        productFacade.findAllActive().size() == 1

        when:
        mockMvc
                .perform(delete(PRODUCTS_URI + "/" + product.id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())

        then: "the product is no longer returned by the findAll endpoint"
        def findAllResponse = mockMvc.perform(get(PRODUCTS_URI)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().response.contentAsString
        mapper.readValue(findAllResponse, List).size() == 0

        and: "the product is still in the database with inactive status"
        !productRepository.getOne(product.id).active
    }

    def "Should return 404 when trying to delete a non existent product"() {
        given:
        createProduct("name", "3.40")
        productFacade.findAllActive().size() == 1

        when:
        def response = mockMvc
                .perform(delete(PRODUCTS_URI + "/42")
                        .contentType(MediaType.APPLICATION_JSON))

        then:
        response.andExpect(status().isNotFound())
    }

    private Product getEntityById(Long id) {
        productRepository.getOne(id)
    }

    private ProductResponse getProductByName(List<ProductResponse> productList, String name) {
        productList.stream()
                .filter({ it.name == name })
                .findFirst().get()
    }

}
