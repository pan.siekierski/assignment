package home.assignment.application

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import home.assignment.IntegrationTest
import home.assignment.application.web.OrderController
import home.assignment.domain.order.OrderFacade
import home.assignment.domain.order.OrderRepository
import home.assignment.domain.order.dtos.OrderDto
import home.assignment.domain.order.dtos.PlaceOrderItemRequest
import home.assignment.domain.order.dtos.PlaceOrderRequest
import home.assignment.domain.order.dtos.PlaceOrderResponse
import home.assignment.domain.product.dtos.UpdateProductRequest
import java.time.format.DateTimeFormatter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType

class OrderIntegrationTest extends IntegrationTest {

    public static final String VALID_EMAIL = "test@mailinator.com"
    public static final String INVALID_EMAIL = "non-email string"
    public static final String FORMATTED_DATE_NOW = DATE_TIME_NOW.format(DateTimeFormatter.ISO_ZONED_DATE_TIME)
    @Autowired
    OrderRepository orderRepository

    @Autowired
    OrderFacade orderFacade

    @Autowired
    ObjectMapper mapper

    def "Should create an order"() {
        given: "two products in the database"
        def product1 = createProduct("product1", "3.14")
        def product2 = createProduct("product1", "4.19")
        def orderItem1 = PlaceOrderItemRequest.of(product1.id, 1)
        def orderItem2 = PlaceOrderItemRequest.of(product2.id, 2)
        def order = PlaceOrderRequest.of([orderItem1, orderItem2], VALID_EMAIL)

        when:
        def response = mockMvc
                .perform(post(OrderController.ORDERS_URI)
                        .content(mapper.writeValueAsString(order))
                        .contentType(MediaType.APPLICATION_JSON))

        then: "status is correct"
        response.andExpect(status().isCreated())

        and: "response has an id"
        def placeOrderResponse = mapper.readValue(response.andReturn().response.contentAsString, PlaceOrderResponse)
        placeOrderResponse.id != 0

        and: "order is present in the database"
        def orderEntity = orderRepository.getOne(placeOrderResponse.id)
        orderEntity.id == placeOrderResponse.id
        orderEntity.buyersEmail.value == VALID_EMAIL
        orderEntity.getOrderItemByProductId(product1.id).get().amount == 1
        orderEntity.getOrderItemByProductId(product1.id).get().price.value == new BigDecimal("3.14")
        orderEntity.getOrderItemByProductId(product2.id).get().amount == 2
        orderEntity.getOrderItemByProductId(product2.id).get().price.value == new BigDecimal("4.19")
    }

    def "Should return 400 on invalid CreateOrderRequest"() {
        given:
        def order = PlaceOrderRequest.of(orderItems as List<PlaceOrderItemRequest>, buyersEmail)

        when:
        def response = mockMvc
                .perform(post(OrderController.ORDERS_URI)
                        .content(mapper.writeValueAsString(order))
                        .contentType(MediaType.APPLICATION_JSON))
        then:
        response.andExpect(status().isBadRequest())

        where:
        buyersEmail   | orderItems
        ""            | [validOrderItem()]
        null          | [validOrderItem()]
        INVALID_EMAIL | [validOrderItem()]
        VALID_EMAIL   | Collections.emptyList()
        VALID_EMAIL   | [PlaceOrderItemRequest.of(1, 0)]
        VALID_EMAIL   | [PlaceOrderItemRequest.of(1, -1)]
        VALID_EMAIL   | [PlaceOrderItemRequest.of(null, -1)]
    }

    def "Should find orders within specified dates"() {
        given:
        def product = createProduct("product1", "15.30")
        def orderItem = PlaceOrderItemRequest.of(product.id, 2)
        def order = PlaceOrderRequest.of([orderItem], VALID_EMAIL)
        def orderId = orderFacade.place(order)
        def foundEntity = orderRepository.getOne(orderId.id)

        when:
        def response = mockMvc
                .perform(get(OrderController.ORDERS_URI)
                        .param("dateFrom", FORMATTED_DATE_NOW)
                        .param("dateTo", FORMATTED_DATE_NOW)
                        .contentType(MediaType.APPLICATION_JSON))

        then: "Found order is correct"
        response.andExpect(status().isOk())
        def contentAsString = response.andReturn().response.contentAsString
        def foundOrders = mapper.readValue(contentAsString, new TypeReference<List<OrderDto>>() {})
        foundOrders.size() == 1
        def foundOrder = foundOrders.get(0)
        foundOrder.id == foundEntity.id
        foundOrder.buyersEmail == foundEntity.buyersEmail.value
        foundOrder.createdAt == DATE_TIME_NOW.toInstant()

        and: "Found order item is correct"
        def foundOrderItem = foundOrder.orderItems.get(0)
        foundOrderItem.price == product.price
        foundOrderItem.amount == orderItem.amount
        foundOrderItem.productId == orderItem.productId
    }

    def "Should not find orders when created order is outside provided date range"() {
        given:
        createProduct("product1", "15.30")
        def searchedDate = DATE_TIME_NOW.plusDays(1).format(DateTimeFormatter.ISO_ZONED_DATE_TIME)

        when:
        def response = mockMvc
                .perform(get(OrderController.ORDERS_URI)
                        .param("dateFrom", searchedDate)
                        .param("dateTo", searchedDate)
                        .contentType(MediaType.APPLICATION_JSON))

        then:
        response.andExpect(status().isOk())
        def contentAsString = response.andReturn().response.contentAsString
        def foundOrders = mapper.readValue(contentAsString, new TypeReference<List<PlaceOrderResponse>>() {})
        foundOrders.size() == 0
    }

    def "Should return code 400 when dateFrom is missing when getting orders"() {
        given:
        createProduct("product1", "15.30")

        when:
        def response = mockMvc
                .perform(get(OrderController.ORDERS_URI)
                        .param("dateTo", FORMATTED_DATE_NOW)
                        .contentType(MediaType.APPLICATION_JSON))

        then:
        response.andExpect(status().isBadRequest())
    }

    def "Should return code 400 when dateTo is missing when getting orders"() {
        given:
        createProduct("product1", "15.30")

        when:
        def response = mockMvc
                .perform(get(OrderController.ORDERS_URI)
                        .param("dateFrom", FORMATTED_DATE_NOW)
                        .contentType(MediaType.APPLICATION_JSON))

        then:
        response.andExpect(status().isBadRequest())
    }

    def "Should retrieve original price from time of the order after product's price change"() {
        given:
        def productBeforeChange = createProduct("product1", "3")
        def placeOrderItemRequest = PlaceOrderItemRequest.of(productBeforeChange.id, 4)
        def placeOrderResponse = orderFacade.place(PlaceOrderRequest.of([placeOrderItemRequest], "irrelevant@google.plh"))
        productFacade.update(productBeforeChange.id, UpdateProductRequest.of(productBeforeChange.name, "10"))

        when:
        def order = orderRepository.getOne(placeOrderResponse.id)

        then:
        order.totalAmount() == new BigDecimal("12")
    }

    PlaceOrderItemRequest validOrderItem() {
        return PlaceOrderItemRequest.of(1, 1)
    }
}
