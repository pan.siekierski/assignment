package home.assignment

import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.data.auditing.DateTimeProvider
import spock.mock.DetachedMockFactory

@TestConfiguration
class TestConfig {

    private static final DetachedMockFactory MOCK_FACTORY = new DetachedMockFactory()

    @Bean
    DateTimeProvider auditingDateTimeProvider() {
        return MOCK_FACTORY.Mock(DateTimeProvider)
    }
}
