package home.assignment.domain.product.model

import spock.lang.Specification

class PriceUnitTest extends Specification {

    def "Should correctly instatiate price"() {
        given:
        Price result = Price.from(new BigDecimal("13.59"))

        expect:
        result.value == new BigDecimal("13.59")
    }

    def "Should throw ArithmeticException when can't create Price from string"() {
        when:
        Price.from(new BigDecimal(priceString))

        then:
        thrown(ArithmeticException)

        where:
        priceString << ["13.412", "0", "-14.1"]
    }

}
