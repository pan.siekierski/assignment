package home.assignment.domain.order.model

import javax.validation.ValidationException
import spock.lang.Specification

class EmailUnitTest extends Specification {

    def "Should create valid email"() {
        when:
        def created = Email.of(email)

        then:
        created.value == email

        where:
        email << ["dra@dta.gf", "sgar@mailinator.com"]
    }

    def "Should throw ValidationException when email invalid"() {
        when:
        Email.of(email)

        then:
        thrown(ValidationException)

        where:
        email << ["53tscr;315@53", "sgardtr"]
    }
}
